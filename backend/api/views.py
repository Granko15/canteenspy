from django.http import JsonResponse
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework.decorators import api_view
from rest_framework import status

@api_view(['GET', 'POST'])
def canteen_list(request):
    if request.method == 'GET':
        canteens = Canteen.objects.all()
        serializer = CanteenSerializer(canteens, many=True)
        return JsonResponse({"canteens": serializer.data}, safe=False)
    if request.method == 'POST':
        serializer = CanteenSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['GET'])
def canteen_detail(request, id):
    try:
        canteen = Canteen.objects.get(pk=id)
    except Canteen.DoesNotExist:
        return JsonResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CanteenSerializer(canteen)
        return JsonResponse(serializer.data)

@api_view(['GET', 'POST'])
def menu_list(request):
    if request.method == 'GET':
        menus = Menu.objects.all()
        serializer = MenuSerializer(menus, many=True)
        return JsonResponse({"menus": serializer.data}, safe=False)
    if request.method == 'POST':
        serializer = MenuSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['GET'])   
def menu_by_canteen_by_date(request, canteen_id, date):
    try:
        canteen = Canteen.objects.get(pk=canteen_id)
    except Canteen.DoesNotExist:
        return JsonResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        menu = Menu.objects.filter(canteen_id=canteen)
        if date:
            menu = menu.filter(day=date)
        serializer = MenuSerializer(menu, many=True)
        return JsonResponse(serializer.data, safe=False)
        
@api_view(['GET', 'POST'])
def rating_list(request):
    if request.method == 'GET':
        menus = Rating.objects.all()
        serializer = RatingSerializer(menus, many=True)
        return JsonResponse({"ratings": serializer.data}, safe=False)
    if request.method == 'POST':
        serializer = RatingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        
@api_view(['GET'])
def ratings_by_menu(request, menu_id):
    try:
        menu = Menu.objects.get(pk=menu_id)
    except Menu.DoesNotExist:
        return JsonResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        ratings = Rating.objects.filter(menu_id=menu)
        serializer = RatingSerializer(ratings, many=True)
        return JsonResponse(serializer.data, safe=False)
    
@api_view(['PUT'])
def menu_add_available(request, menu_id):
    if request.method == 'PUT':
        menu = Menu.objects.get(pk=menu_id)
        menu.available += 1

        serializer = MenuSerializer(menu, data={"name": menu.name})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return JsonResponse(serializer.data)
        
@api_view(['PUT'])
def menu_add_unavailable(request, menu_id):
    if request.method == 'PUT':
        menu = Menu.objects.get(pk=menu_id)
        menu.unavailable += 1

        serializer = MenuSerializer(menu, data={"name": menu.name})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return JsonResponse(serializer.data)